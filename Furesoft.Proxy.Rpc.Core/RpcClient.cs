﻿using Furesoft.Proxy.Rpc.Core.Communicator;
using Furesoft.Proxy.Rpc.Core.Messages;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Furesoft.Proxy.Rpc.Core
{
    public class RpcClient : IDisposable
    {
        private MemoryMappedFileCommunicator sender;

        public RpcClient(string name)
        {
            sender = new MemoryMappedFileCommunicator(name, 50000); ;

            sender.ReadPosition = 0;
            sender.WritePosition = 2500;
            sender.DataReceived += Sender_DataReceived;
        }

        public void Start()
        {
            sender.StartReader();
        }

        private void Sender_DataReceived(object sender, MemoryMappedDataReceivedEventArgs e)
        {
            var response = RpcServices.Deserialize(e.Data);

            if (response is RpcMethodAwnser awnser)
            {
                ReturnValue = awnser.ReturnValue;
            }
            else if (response is RpcExceptionMessage ex)
            {
                throw new RpcException(ex.Interface, ex.Name, new Exception(ex.Message));
            }

            mre.Set();
        }

        object ReturnValue;
        ManualResetEvent mre = new ManualResetEvent(false);

        public object CallMethod<Interface>(string methodname, params object[] args)
            where Interface : class
        {
            mre.Reset();

            var m = new RpcMethod
            {
                Interface = typeof(Interface).Name,
                Name = methodname,
                Args = args.ToList()
            };

            sender.Write(RpcServices.Serialize(m));

            mre.WaitOne();

            return ReturnValue;
        }

        public T CallMethod<Interface, T>(string methodname, params object[] args)
            where Interface : class
        {
            return (T)CallMethod<Interface>(methodname, args);
        }

        public Task<object> CallMethodAsync<Interface>(string methodname, params object[] args)
            where Interface : class
        {
            return Task.Run(() =>
            {
                return CallMethod<Interface>(methodname, args);
            });
        }
        public Task<T> CallMethodAsync<Interface, T>(string methodname, params object[] args)
            where Interface : class
        {
            return Task.Run(() =>
            {
                return CallMethod<Interface, T>(methodname, args);
            });
        }


        public void SetProperty<Interface>(string propname, object value)
            where Interface : class
        {
            CallMethod<Interface>($"set_{propname}", value);
        }

        public void SetIndex<Interface>(object[] indizes, object value)
        {
            mre.Reset();

            var m = new RpcIndexMethod
            {
                Name = "set_Index",
                Interface = typeof(Interface).Name,
                Indizes = indizes,
                Value = value
            };

            sender.Write(RpcServices.Serialize(m));

            mre.WaitOne();
        }
        public object GetIndex<Interface>(object[] indizes)
        {
            mre.Reset();

            var m = new RpcIndexMethod
            {
                Interface = typeof(Interface).Name,
                Name = "get_Index",
                Indizes = indizes
            };

            sender.Write(RpcServices.Serialize(m));

            mre.WaitOne();

            return ReturnValue;
        }

        public object GetProperty<Interface>(string propertyname)
            where Interface : class
        {
            return CallMethod<Interface>($"get_{propertyname}");
        }

        public dynamic Bind<Interface>()
            where Interface : class
        {
            return new InterfaceProxy<Interface>(this, false);
        }

        public dynamic BindAsync<Interface>()
            where Interface : class
        {
            return new InterfaceProxy<Interface>(this, true);
        }

        public void Dispose()
        {
            sender.Dispose();
        }
    }
}