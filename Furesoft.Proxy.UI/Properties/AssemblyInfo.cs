﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Markup;

[assembly: AssemblyTitle("Furesoft.Proxy.UI")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Furesoft.Proxy.UI")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

[assembly: Guid("0d45711e-16a9-45c2-b8b3-a4b62a041f4d")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: XmlnsDefinition("http://furesoft.app/proxy", "Furesoft.Proxy.UI", AssemblyName = "Furesoft.Proxy.UI")]
[assembly: XmlnsDefinition("http://furesoft.app/proxy", "Furesoft.Proxy.UI.Converter", AssemblyName = "Furesoft.Proxy.UI")]
